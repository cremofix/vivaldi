# vivaldi#


** DAS IST EIN BASTELPROJEKT !!! NICHT ZUR UNGEPRÜFTEN NACHAHMUNG GEEIGNET !! **

** Steuerung einer La Spaziale ViValdi S1 mit Raspberry **

Nachdem im Frühjahr 2020 die Elektronik meiner Vivaldi aus unerklärlichen Gründen den Geist aufgab und
der Fachhändler nicht in der Lage war, ein verbindlich passendes Ersatzteil zu liefern sondern nur ein vermutlich passendes ohne Rückgabemöglichkeit bei Nichtpassen, das ganze dann auch noch zu einem horrenden Preis und  mit unbestimmter Lieferzeit wegen Corona, habe ich mich entschlossen, die gesamte Steuerung der Vivaldi auf Basis eines Microcontolers selber aufzubauen.

Ich bin kein Elektriker, sondern nur leidenschaflticher Hobbybastler und Autodidakt.
 
Das ist mein erstes Leiterplattenprojekt. Ein versierter Elektronik-Fachmann würde vieles sicher anders und bestimmt besser machen.

Da in der Maschine auch 230V Netzspannung anliegt, besteht beim Nachmachen Lebensgefahr. Den 230V Teil habe ich 1:1 von der alten Elektroik übernommen. Daher hoffe ich mal, dass er nicht unsicherer als das Original ist. :-)

** Dennoch : Nachahmung ausdrücklich auf eigene Gefahr. **

Ich übernehme keine Garantie oder Haftung für Schäden, die durch Nachbau entstehen!

Für Anregungen, wie man das ganze besser machen kann, bin ich jederzeit dankbar.

Ich feue mich dennoch, dass dieses Erstlingsprojekt funktioniert und stelle alle Dateien unter GPL zur Verfügung.

Weiterentwicklung durchaus gewünscht.
Was ich noch plane:

1. Weboberfläche
2. PID-Steuerung für die Brühtempereatur optimeiren
3. Reinigungsprogramm
4. Steuerung von Stdby, Reboot, Shutdown über Boilertaste
5. Optimierung Boilerbefüllung

Anmerkung: 2021-07-09: 
Die hier zur Verfügung gestellte Schaltung ist mit leichten Korrekturen bei mir im Einsatz und funktioniert im Großen und ganzen ganz ordentlich. Vieles im Projekt ist noch nicht endgültig fertig. Die Schaltpläne müssen noch angepasst werden es ist noch ein Fehler in der Verschaltung des Temperatursensors. Der Footprint für die Entsördrossel ist noch zu groß und muss korrigiert werden. Bei der Pulldown Verschaltung des Displays ist aktuell 1 Widerstand pro 5 Dioden eingezeichnet. Ich würde in der nächsten Version jeder Leitung einen eigenen geben. Ich habe die Fehler auf meiner Platine durch Auftrennen einer Leiterbahn und mit etwas Draht manuell korrigiert. Das Platinenlayout also nicht einfach so zur Fertigung geben.

Ich komme aktuell leider nicht dazu, das Projekt weiter zu bringen. Vielelicht im Winter wieder :-)
Da mich inzwischen mehrere Leute gebeten haben, das projekt offen zu legen, stelle ich es jetzt schon online.

Aber, wie gesagt mit der klaren Ansage. ** Es ist noch nicht fertig **

Wenn Ihr das Projekt weiterentwickelt oder optimiert. Stellt Eure Ergebnisse bitt ewieder der Öffentlichkeit zur Verfügung oder teilt sie mir mit, ich shcaue dann, ob ich sie einbindne kann.

Viel Spaß

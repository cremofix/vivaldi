#!/usr/bin/python3
import signal
import sys
#from gpiozero import LED, Button
import RPi.GPIO as GPIO
import configparser
import math
import time
import board
import busio
import digitalio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import threading

def signal_handler(sig, frame):
        GPIO.cleanup()
        sys.exit(0)


# Konstanten
r2=22000 # Festwiderstand AnalogP2
u0=3.3  # Grundspannung Analog
k1brew=207000 # const 1 for thermosensor brew
k2brew=4032 # const 2 for thermosensor brew
k1boil=195000 # const 1 for thermosensor boil
k2boil=4040 # const 2 for thermosensor boil
k3=1/198.15 # Thermokonst
k4=273.15 # 0 C in Kelvin

d1=0
d1blink=0
d2=16 
d2blink = d2 & ~16
mode='off'
myd1=d1
myd2=d2
brewing=False

def btn1C_pressed_callback(channel):
    print("1 Cups pressed!")
def btn2C_pressed_callback(channel):
    global brewing
    if brewing:
        MVPump.value=True
        MVBrew.value=True
        brewing=False
    else: 
        MVPump.value=False
        MVBrew.value=False
        brewing=True
    print("2 Cups pressed!")
def btnWat_pressed_callback(channel):
    print("Water pressed!")
    #MVHotWater.value= not MVHotWater.value
def btnBoi_pressed_callback(channel):
    print("Boil pressed!")
def btnOn_pressed_callback(channel):
    global mode
    global d2
    print("On/Off pressed!")
    if mode == 'on':
        mode = 'off'
        d2 = d2 & ~16 
        #d2blink = d2 & ~16
    else:
        mode = 'on'
        d2 = d2 | 16 
        #d2blink = d2

# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)
# Create the ADC object using the I2C bus
ads = ADS.ADS1015(i2c)
# Create single-ended input for Brew- and BoilTemp
chanbr= AnalogIn(ads, ADS.P0) # Thermosensor Brew
chanbo= AnalogIn(ads, ADS.P2) # Thermosensor Boil

config = configparser.ConfigParser()

#definition for LED-Array
base1 = digitalio.DigitalInOut(board.D26)
base1.direction = digitalio.Direction.OUTPUT
base2 = digitalio.DigitalInOut(board.D27)
base2.direction = digitalio.Direction.OUTPUT
base1.value=False
base2.value=False
led=[]
led.append(digitalio.DigitalInOut(board.D17)) 
led.append(digitalio.DigitalInOut(board.D22)) 
led.append(digitalio.DigitalInOut(board.D23)) 
led.append(digitalio.DigitalInOut(board.D24)) 
led.append(digitalio.DigitalInOut(board.D25)) 
for i in range(5):
    led[i].direction = digitalio.Direction.OUTPUT
    led[i].value = True
    # 1:91/96; 2:92/97; 3:93/econ; 4:94/boiler; 5:95/on

#Array for Flowmeter-Settings
cupticks=[]
cupticks.append(100) # flowmeter ticks for 1 cup
cupticks.append(200) # flowmeter ticks for 2 cups

def setleds(n):
    for i in range(5):
        if n&(2**i)==(2**i):
            led[i].value=False
        else:
            led[i].value=True
            
GPIO.setmode(GPIO.BCM)
#Button definition On/Off
GPIO.setup(9,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(9, GPIO.FALLING, callback=btn2C_pressed_callback, bouncetime=300)
#Button definition 2 Cups
GPIO.setup(11,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(11, GPIO.FALLING, callback=btnOn_pressed_callback, bouncetime=100)
#Button definition 1 Cup
#GPIO.setup(8,GPIO.IN, pull_up_down=GPIO.PUD_UP)
#GPIO.add_event_detect(8, GPIO.FALLING, callback=btn1C_pressed_callback, bouncetime=100)
#Button definition Boiler
GPIO.setup(10,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(10, GPIO.FALLING, callback=btnBoi_pressed_callback, bouncetime=100)
#Button definition Water
#GPIO.setup(7,GPIO.IN, pull_up_down=GPIO.PUD_UP)
#GPIO.add_event_detect(7, GPIO.FALLING, callback=btnWat_pressed_callback, bouncetime=100)
#btnWat = Button(7)
#btnWat.when_pressed=btnWat_pressed_callback


#removed for test with GPIO
#btnBoil = digitalio.DigitalInOut(board.D10)              # boiler
#btnBoil.direction = digitalio.Direction.INPUT
#btnBoil.pull = digitalio.Pull.UP
#btnOnOff = digitalio.DigitalInOut(board.D11)              # on/off
#btnOnOff.direction = digitalio.Direction.INPUT
#btnOnOff.pull = digitalio.Pull.UP
#btnTwoCups = digitalio.DigitalInOut(board.D9)              # 2cups
#btnTwoCups.direction = digitalio.Direction.INPUT
#btnTwoCups.pull = digitalio.Pull.UP
#btnOneCup = digitalio.DigitalInOut(board.D8)              # 1cup
#btnOneCup.direction = digitalio.Direction.INPUT
#btnOneCup.pull = digitalio.Pull.UP
#btnWater = digitalio.DigitalInOut(board.D7)              # water
#btnWater.direction = digitalio.Direction.INPUT
#btnWater.pull = digitalio.Pull.UP

sensWater = digitalio.DigitalInOut(board.D16)              # watersensor 1=empty 0=full
sensWater.direction = digitalio.Direction.INPUT
sensWater.pull = digitalio.Pull.UP

MVPump = digitalio.DigitalInOut(board.D1) # Relais 3
MVPump.direction = digitalio.Direction.OUTPUT
MVBrew = digitalio.DigitalInOut(board.D0) # Relais 4
MVBrew.direction = digitalio.Direction.OUTPUT
MVWater = digitalio.DigitalInOut(board.D5) # Relais 1 
MVWater.direction = digitalio.Direction.OUTPUT
MVHotWater = digitalio.DigitalInOut(board.D4) # Relais 2
MVHotWater.direction = digitalio.Direction.OUTPUT
MVBrew.value=True
MVWater.value=True
MVHotWater.value=True
MVPump.value=True
mode='off'
HeaterBrew = digitalio.DigitalInOut(board.D12)
HeaterBrew.direction = digitalio.Direction.OUTPUT
HeaterBoil = digitalio.DigitalInOut(board.D13)
HeaterBoil.direction = digitalio.Direction.OUTPUT

def ledflow():
    global d1
    global d2
    for j in range(8):
      if j<5:
          d2=0
          d1=2**j
      else:
          d1=0
          d2=2**(j-5)
      time.sleep(0.1)

def resistance(u):
#    res =  (u*r2)/(u0-u) 
    res =  ((u0-u)*r2)/(u) 
    return res 

def temp(r,k1,k2):
    t = 1/(math.log(r/1000/k1)/k2+k3)-k4
    return t 

def brew(cups):
    global brewing
    if brewing==True:
        print('ausschalten')
        MVBrew.value=True
        MVPump.value=True
        brewing=False
    else:    
        print('brewing')
        count=0
        MVBrew.value=False
        MVPump.value=False
        brewing=True
        lasttick=sensWater.value
#        while count <= cupticks[cups-1] and brewing==True:
#            tick=sensWater.value
#            if tick != lasttick:
#                count = count+1
#                lasttick=tick 
#        MVBrew.value=True
#        MVPump.value=True
#        brewing=False

class MyTempWatch (threading.Thread):
    def run (self):
        while True:
            global k1brew
            global k2brew
            global k1boil
            global k2boil
            global mode
            global d1
            global d2
            global d1blink
            global d2blink
            boilvolt=chanbo.voltage
            brewvolt=chanbr.voltage
            if mode=='on':
                tbrewsoll=93
                tboilsoll=120
            elif mode=='standby':
                tbrewsoll=40
                tboilsoll=50
            else:
                tbrewsoll=0
                tboilsoll=0
            if brewvolt>0:
                brewtemp=temp(resistance(brewvolt),k1brew,k2brew)
                if brewtemp < (tbrewsoll-0.5) and mode != 'off':
                    HeaterBrew.value = True
                    brewheat='on'
                elif brewtemp > (tbrewsoll) or mode == 'off':
                    HeaterBrew.value = False  # muss zum Ausschaltennoch in true geändert werden NUR TEST
                    brewheat='off'
            else:
                HeaterBrew.value = True # Emergency OFF
            if boilvolt>0:    
                boiltemp=temp(resistance(boilvolt),k1boil,k2boil)
                if boiltemp < tboilsoll and mode != 'off':
                    HeaterBoil.value = True
                    boilheat='on'
                elif boiltemp > (tboilsoll+0.5) or mode == 'off':
                    HeaterBoil.value = False
                    boilheat='off'
            else:   
                HeaterBoil.value = True # Emergency OFF
                print('Fehler: Vboil = 0 - Thermofühler defekt!')
            print("{:>5.3}\t{:>5.3f}\t{:>5.3}\t{:>5.3}\t{:>5.3f}\t{:>5.3}\t{:>5.3}\t{:>1}".format('BrewTemp',brewtemp,brewheat,'BoilTemp',boiltemp,boilheat,mode,sensWater.value))
            if brewheat == 'off':
                d1 = d1 & ~31 #turn off LED 91-95
                d2 = d2 & ~3 # turn off LED 96,97
            else:
                if brewtemp < 91:
                    d1 = 1
                    d1blink = 0
                    d2 = d2 & ~3
                    d2blink = d2blink & ~3
                elif brewtemp < 92:
                    d1 = 3
                    d1blink = 1
                    d2 = d2 & ~3
                    d2blink = d2blink & ~3
                elif brewtemp < 93:
                    d1 = 7
                    d1blink = 3
                    d2 = d2 & ~3
                    d2blink = d2blink & ~3

            if boilheat == 'off':
                d2 = d2 & ~8 # turn off LED Boil
            else:
                d2 = d2 | 8
                d2blink = d2blink & ~8
            time.sleep(1)

class MyDisplayWatch (threading.Thread):
    def run (self):
        global myd1
        global myd2
        while True:
            #myd1=d1
            #myd2=d2
            base2.value=False
            setleds(myd1)
            base1.value=True
            time.sleep(0.005)
            base1.value=False
            setleds(myd2)
            base2.value=True
            time.sleep(0.005)

class MyBoilFillWatch(threading.Thread):
    def run (self):
       global brewing
       global mode
       if mode=='on' and not brewing:
           if sensWater.value==1:
              MVPump.value=False
              MVWater.value=False
              sleep(2)
       else:
           MVPump.value=True
           MVWater.value=True
            
TempThread = MyTempWatch()
DisplayThread = MyDisplayWatch()
BoilThread = MyBoilFillWatch()

TempThread.setName("TempThread")
DisplayThread.setName("DisplayThread")
BoilThread.setName("BoilThread")
signal.signal(signal.SIGINT, signal_handler)

TempThread.start()
DisplayThread.start()
BoilThread.start()
TempThread.join()
DisplayThread.join()
BoilThread.join()
signal.pause()

disptime = time.perf_counter()

#while true:
#    if time.perf_counter() - disptime >= 1.0:
#        if blinking:
#            myd1=d1
#            myd2=d2
#        else:
#            myd1=d1blink
#            myd2=d2blink
#        disptime = time.perf_counter()



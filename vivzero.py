#!/usr/bin/python3
import signal
import sys
from subprocess import check_call
from gpiozero import LED, Button
import configparser
import math
import os.path
import time
import board
import busio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import threading

# Konstanten VORSICHT! NUR VERÄNDERN, WENN SICHER!!
r2=22000 # Festwiderstand AnalogP2
u0=3.3  # Grundspannung Analog
k1brew=207000 # const 1 for thermosensor brew
k2brew=4032 # const 2 for thermosensor brew
k1boil=195000 # const 1 for thermosensor boil
k2boil=4040 # const 2 for thermosensor boil
k3=1/198.15 # Thermokonst
k4=273.15 # 0 C in Kelvin
preshot=2.0
# PID Konstanten erste grobe Annaeherung 
PIDkp=1200.0
PIDki=40.0
PIDkd=20.0
refillmaxtime=15.0

# Variablen Vorbelegung
d1=0
d1blink=0
d2=0 
d2blink=0
mode='off'
action='off'
brewmode='timer' # flow, timer, manual
brewmaxtime=30.0
brewpressed=False
refillpressed=False
myd1=d1
myd2=d2
brewing=False
flow=0
lastInput=15.0
lastTime=0.0
LastLogTime=0.0
LastWSensTime=0.0
PIDStartTime=time.time()*1000
output = 0.0
maxflow = 0
verbosity=2
LogInterval=10.0
WSensInterval=60.0
brewheat='off'
boilheat='off'
brewtemp=0.0
boiltemp=0.0

# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)
# Create the ADC object using the I2C bus
ads = ADS.ADS1015(i2c)
# Create single-ended input for Brew- and BoilTemp
chanbr= AnalogIn(ads, ADS.P0) # Thermosensor Brew
chanbo= AnalogIn(ads, ADS.P2) # Thermosensor Boil

#config = configparser.ConfigParser()

#definition for LED-Array
base1 = LED(26)
base2 = LED(27)
base1.off()
base2.off()

led=[]
led.append(LED(17,active_high=False)) 
led.append(LED(22,active_high=False)) 
led.append(LED(23,active_high=False)) 
led.append(LED(24,active_high=False)) 
led.append(LED(25,active_high=False)) 

for i in range(5):
    led[i].off()
    # 1:91/96; 2:92/97; 3:93/econ; 4:94/boiler; 5:95/on

def setleds(n):
    for i in range(5):
        if n&(2**i)==(2**i):
            led[i].on()
        else:
            led[i].off()

#Array for Flowmeter-Settings
cupticks=[]
cupticks.append(68) # flowmeter ticks for 1 cup
cupticks.append(135) # flowmeter ticks for 2 cups

#Button definition
BtnOnOff = Button(11,pull_up=True,bounce_time=0.1)
Btn2Cups = Button(9,pull_up=True,bounce_time=0.1)
Btn1Cup = Button(8,pull_up=True,bounce_time=0.1)
BtnBoil = Button(10,pull_up=True,bounce_time=0.1)
BtnWater = Button(7,pull_up=True,bounce_time=0.1)

preventrefill=True 
sensWater = Button(16,pull_up=False,bounce_time=3.0)    # watersensor 0=empty 1=full
FlowMeter = Button(6,pull_up=True,bounce_time=0.1)    

MVPump = LED(1,active_high=False) # Relais 3
MVBrew = LED(0,active_high=False) # Relais 4
MVWater = LED(5,active_high=False) # Relais 1 
MVHotWater = LED(4,active_high=False) # Relais 2
MVBrew.off()
MVWater.off()
MVHotWater.off()
MVPump.off()

HeaterBrew = LED(12)
HeaterBoil = LED(13)
HeaterBrew.off()
HeaterBoil.off()

def all_off():
    action='off'
    MVBrew.off()
    MVWater.off()
    MVHotWater.off()
    MVPump.off()
    HeaterBrew.off()
    HeaterBoil.off()

def quit_program():
    all_off()
    sys.exit('ByeBye')

def setTempDisp(t,blink):
    global d1,d2,d1blink,d2blink
    if t < 91.0:
        d1=1
        d1blink=0

def activate_WSens(state):
    global sensWater
    sensWater.close()
    if state == 'up':
        sensWater = Button(16,pull_up=True,bounce_time=3.0)    # watersensor 0=empty 1=full
    else:
        sensWater = Button(16,pull_up=False,bounce_time=3.0)    # watersensor 0=empty 1=full

def signal_handler(sig, frame):
    all_off()
    sys.exit(0)

def shutdown():
    all_off()
    #check_call(['sudo', 'poweroff'])
    mylog('This would be a shutdown')

def refill():
    global refillpressed, action
    if mode == 'on' and action != 'brewing' and not preventrefill:
        action = 'refill'
        refillpressed = True

def flowcount():
    global flow
    #mylog ("flow")
    flow=flow+1

# lights leds from 1 to 10 - not in use yet
def ledflow():
    global d1, d2
    for j in range(8):
      if j<5:
          d2=0
          d1=2**j
      else:
          d1=0
          d2=2**(j-5)
      time.sleep(0.1)

def resistance(u):
    res =  ((u0-u)*r2)/(u) 
    return res 

def temp(r,k1,k2):
    t = 1/(math.log(r/1000/k1)/k2+k3)-k4
    return t 

def mylog(s):
    if verbosity > 0 and mode == 'on':
       tlog = open("vivaldi.log", "a")
       tlog.write(time.strftime("%Y-%m-%d %H:%M:%S> ",time.localtime())+s+"\n")
       tlog.close()
    if verbosity > 1:
       print(s)

def PIDtemp(myInput,mySetpoint):
    global lastTime, lastInput, output
    POnE=True
    error=0.0
    outputSum=0.0
    PIDmin=0
    PIDmax=3000
    SampleTime = 3000.0
    now = time.time()*1000
    timeChange = now-lastTime
    if timeChange >= SampleTime:
        error = mySetpoint - myInput
        dInput = myInput - lastInput
        outputSum += (PIDki*error)
        if not POnE:
            outputSum-= PIDkp * dInput
        if outputSum > PIDmax:
            outputSum=PIDmax
        elif outputSum < PIDmin:
            outputSum=PIDmin
        if POnE:
            output = PIDkp*error
        else:
            output=0
        output += outputSum - PIDkd*dInput
        if output > PIDmax:
            output=PIDmax
        elif output < PIDmin:
            output=PIDmin
        lastInput=myInput
        lastTime=now
        return True
    else:
        return False

def brew1():
    global action, brewpressed, flow, maxflow
    if action == 'brewing':
        mylog('Brew off')
        action = 'off'
    else:    
        flow=0
        maxflow=cupticks[0]
        mylog("brewing 1 Cup")
        action = 'brewing'
        brewpressed=True
def brew2():
    global action, brewpressed, flow, maxflow
    if action == 'brewing':
        mylog('Brew off')
        action = 'off'
    else:    
        flow=0
        maxflow=cupticks[1]
        mylog("brewing 2 Cups")
        action = 'brewing'
        brewpressed=True

def OnOff():
    global mode,sensWater
    mylog("On/Off pressed!")
    if mode == 'on':
        mode = 'off'
        #sensWater.close()
        #sensWater = Button(16,pull_up=False,bounce_time=3.0)    # watersensor 0=empty 1=full

    else:
        mode = 'on'
        #sensWater.close()
        #sensWater = Button(16,pull_up=True,bounce_time=3.0)    # watersensor 0=empty 1=full

def Standby():
    global mode
    mylog("Standby pressed!")
    mode = 'standby'

def HotWater():
      global action
      if mode == 'on': 
        if action == 'HotWater':
          mylog("Hot Water off")
          action = 'off'
        elif action != 'brewing' and action != 'refill':
          action = 'HotWater'
          mylog("Hot Water on")

def Boil_pressed():
      global action
      if action == 'refill':
        action = 'refillstop'
      else:
        action = 'off'  
      mylog("Boil pressed!")

Btn1Cup.when_pressed = brew1
Btn2Cups.when_pressed = brew2
BtnOnOff.when_pressed = OnOff
BtnWater.when_pressed = HotWater
BtnBoil.when_pressed = Boil_pressed
#sensWater.when_released = refill
FlowMeter.when_pressed = flowcount

class MyTempWatch (threading.Thread):
    def run (self):
        while True:
            global k1brew, k2brew, k1boil, k2boil, mode, d1, d2, d1blink, d2blink, PIDStartTime, output, flow, brewheat, boilheat, brewtemp, boiltemp
            #brewheat='off'
            #boilheat='off'
            now = 0.0
            boilvolt=chanbo.voltage
            brewvolt=chanbr.voltage
            if brewvolt>0:
              brewtemp = temp(resistance(brewvolt),k1brew,k2brew)
            else:
              mylog('Fehler: Vbrew = 0 - Thermofühler defekt!')
              HeaterBrew.off() # Emergency OFF
              brewheat='off'
            if boilvolt>0:    
              boiltemp=temp(resistance(boilvolt),k1boil,k2boil)
            else:   
              HeaterBoil.off() # Emergency OFF
              boilheat='off'
              mylog('Fehler: Vboil = 0 - Thermofühler defekt!')
            if mode=='on':
                tbrewsoll=93
                tboilsoll=120
            elif mode=='standby':
                tbrewsoll=40
                tboilsoll=50
            else:
                tbrewsoll=0
                tboilsoll=0

            if mode != 'off':
                PIDsuccess = PIDtemp(brewtemp,tbrewsoll)
                if PIDsuccess:
                    ptemp=output
                now=time.time()*1000
                if (now-PIDStartTime)>3000:
                    PIDStartTime = now
                if output > (now-PIDStartTime):
                    HeaterBrew.on()
                    brewheat='on'
                else:
                    HeaterBrew.off()
                    brewheat='off'
            
                if boiltemp < tboilsoll and mode != 'off':
                    HeaterBoil.on()
                    d2 = d2 | 8
                    d2blink = d2blink & ~8
                    boilheat='on'
                elif boiltemp > (tboilsoll+0.5) and mode != 'off':
                    HeaterBoil.off()
                    boilheat='off'
                    d2 = d2 | 8
                    d2blink = d2blink | 8
                else:
                    HeaterBoil.off()
                    boilheat='off'
                if mode == 'on':
                 if brewtemp < 91:
                    d1 = 1
                    d1blink = 0
                    d2 = d2 & ~7
                    d2blink = d2blink & ~7
                 elif brewtemp < 92:
                    d1 = 2 
                    d1blink = 0
                    d2 = d2 & ~7
                    d2blink = d2blink & ~7
                 elif brewtemp < 93:
                    d1 = 4
                    d1blink = 0
                    d2 = d2 & ~7
                    d2blink = d2blink &  ~7
                 elif brewtemp < 94:
                    d1 = 8
                    d1blink = 0
                    d2 = d2 &  ~7
                    d2blink = d2blink & ~7
                 elif brewtemp < 95:
                    d1 = 16
                    d1blink = 0
                    d2 = d2 & ~7
                    d2blink = d2blink & ~7
                 elif brewtemp < 96:
                    d1 = 0
                    d1blink = 0
                    d2 = d2 | 1
                    d2blink = d2blink & ~7
                 elif brewtemp <= 97:
                    d1 = 0
                    d1blink = 0
                    d2 = d2 | 2
                    d2blink = d2blink & ~7
                 else:
                    d1 = 0
                    d1blink = 0
                    d2 = d2 | 4
                    d2blink = d2blink | 4 

                elif mode == 'standby':
                  mode=mode  
            else:
              HeaterBoil.off()
              HeaterBrew.off()
              d1 = 0
              d1blink = 0
              d2 = 0
              d2blink= 16

class MyDisplayWatch (threading.Thread):
    def run (self):
        global myd1, myd2, mode, d1, d2, d1blink, d2blink
        mylasttime=time.monotonic()
        while True:
            if mode == 'on':
                blinktime_on = 0.3
                blinktime_off = 1.3
            elif mode == 'standby':
                blinktime_on = 0.1
                blinktime_off = 2.3
            else:
                blinktime_on = 0.1
                blinktime_off = 5.0

            if myd1 == d1 and myd2 == d2:
                blinktime=blinktime_on
                if time.monotonic()-mylasttime > blinktime:
                    myd1=d1blink
                    myd2=d2blink
                    mylasttime=time.monotonic()
            else:
                blinktime=blinktime_off
                if time.monotonic()-mylasttime > blinktime:
                    myd1=d1
                    myd2=d2
                    mylasttime=time.monotonic()
            base2.off()
            setleds(myd1)
            base1.on()
            time.sleep(0.005)
            base1.off()
            setleds(myd2)
            base2.on()
            time.sleep(0.005)

class MyMain(threading.Thread):
    def run (self):
      global preshot,flow, action, refillpressed, brewpressed, brewmode, mode, d2, d2blink, LastLogTime, LogInterval, LastWSensTime, WSensInterval, brewheat, boilheat, brewtemp, boiltemp, flow
      while True:
          now=time.time()
          mylogtxt="Mod: {:>3}  Act: {:>7}  Brew: {:>3} {:>5.2f}  Boil: {:>3} {:>6.2f}  WSens: {:>1} Flow: {:>3}".format(mode,action,brewheat,brewtemp,boilheat,boiltemp,sensWater.value,flow)
          if (now-LastLogTime)>LogInterval:
              mylog(mylogtxt)  
              LastLogTime=now
          if (now-LastWSensTime)>WSensInterval:
              #mylog("measuring water")
              #activate_WSens('on')
              LastWSensTime=now
          if os.path.isfile('/home/pi/vivaldi/vivtimer'):
            f = open("/home/pi/vivaldi/vivtimer","r")
            mytxt=f.readline().rstrip()
            f.close()
            os.remove("vivtimer")
            if mytxt == 'on':
              mode = 'on'
            elif mytxt == 'off':
              mode = 'off'
              action = 'off'
          if mode == 'on':
             d2 = d2 | 16 
             d2blink = d2blink | 16
             if action == 'brewing':
                 MVWater.off()
                 MVHotWater.off()
                 now=time.time()
                 MVBrew.on()
                 if brewpressed:
                    brewpressed=False
                    brewstart=now
                 if now-brewstart > preshot:
                     MVPump.on()
                 if brewmode == "timer":
                     if now-brewstart > brewmaxtime:
                         MVPump.off()
                         MVBrew.off()
                         action = 'off'
                 elif brewmode == "flow":
                     if flow >= maxflow:
                         MVPump.off()
                         MVBrew.off()
                         action = 'off'
             elif action == 'refill':
                 MVBrew.off()
                 MVHotWater.off()
                 now=time.time()
                 MVWater.on()
                 MVPump.on()
                 if refillpressed:
                     refillpressed = False
                     refillstart=now
                 if now-refillstart > refillmaxtime:
                     MVPump.off()
                     MVWater.off()
                     action = 'off'
             elif action == 'refillstop':
                 MVPump.off()
                 MVWater.off()
             elif action == 'HotWater':
                 MVBrew.off()
                 MVWater.off()
                 MVPump.off()
                 MVHotWater.on()
             else:
                 MVWater.off()
                 MVHotWater.off()
                 MVBrew.off()
                 MVPump.off()
             if sensWater.value == 0:
                 refill()    
          elif mode == 'standby':
             d2 = d2 | 16 
             d2blink = d2blink | 16
          else:
             d2 = d2 | 16 
             d2blink = d2blink & ~16

MainThread = MyMain()
TempThread = MyTempWatch()
DisplayThread = MyDisplayWatch()

MainThread.setName("MainThread")
TempThread.setName("TempThread")
DisplayThread.setName("DisplayThread")
signal.signal(signal.SIGINT, signal_handler)

MainThread.start()
TempThread.start()
DisplayThread.start()

MainThread.join()
TempThread.join()
DisplayThread.join()

signal.pause()

disptime = time.perf_counter()

